import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { StatsService } from './databases/stats/stats.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, StatsService],
})
export class AppModule {}
