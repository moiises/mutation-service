import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { StatsService } from './databases/stats/stats.service';
import { IStats } from './interfaces/stats.interface';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly statsService: StatsService,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('mutation')
  hasMutation(@Body() body: any): { hasMutation: boolean } {
    const response = { hasMutation: this.appService.hasMutation(body.dna) };

    if (response.hasMutation) {
      this.statsService.addCountMutations();
    } else {
      this.statsService.addCountNoMutations();
    }

    return response;
  }

  @Get('stats')
  getStats(): IStats {
    return this.statsService.getStats();
  }
}
