import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  /**
   * The function getHello() returns a string
   * @returns A string.
   */
  getHello(): string {
    return 'Hello World!';
  }

  /**
   * It iterates over the dna array, and for each element, it iterates over the string, and for each
   * character, it checks if there's a horizontal, vertical, or diagonal sequence of 4 identical
   * characters
   * @param {string[]} dna - an array of strings representing the rows of the matrix
   * @returns A boolean value.
   */
  hasMutation(dna: string[]): boolean {
    const size = dna.length;
    for (let i = 0; i < size; i++) {
      for (let j = 0; j < dna[i].length; j++) {
        const word = dna[i][j];

        // horizontal
        if (j <= dna[i].length - 4 && dna[i].includes(word.repeat(4))) {
          return true;
        }

        // vertical
        const vertical = dna
          .map((str) => {
            return str[j];
          })
          .join('');

        if (i <= size - 4 && vertical.includes(word.repeat(4))) {
          return true;
        }

        // diagonales
        const oblicuo1 = dna
          .slice(i)
          .map((str, strIndex) => {
            return str[j + strIndex];
          })
          .join('');

        if (oblicuo1.includes(word.repeat(4))) {
          return true;
        }

        const oblicuo2 = dna
          .slice(i)
          .map((str, strIndex) => {
            return str.split('').reverse().join('')[j + strIndex];
          })
          .join('');

        if (oblicuo2.includes(word.repeat(4))) {
          return true;
        }
      }
    }
    return false;
  }
}
