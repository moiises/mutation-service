import { Injectable } from '@nestjs/common';
import { IStats } from '../../interfaces/stats.interface';

@Injectable()
export class StatsService {
  private stats: IStats = {
    count_mutations: 0,
    count_no_mutations: 0,
    ratio: 0,
  };

  getStats(): IStats {
    return this.stats;
  }

  addCountMutations(): void {
    this.stats.count_mutations += 1;
    this.stats.ratio =
      this.stats.count_mutations / this.stats.count_no_mutations;
  }

  addCountNoMutations(): void {
    this.stats.count_no_mutations += 1;
    this.stats.ratio =
      this.stats.count_mutations / this.stats.count_no_mutations;
  }
}
