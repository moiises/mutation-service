import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { StatsService } from './databases/stats/stats.service';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService, StatsService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(appController.getHello()).toBe('Hello World!');
    });

    it('should hasMutation return false', () => {
      const { hasMutation } = appController.hasMutation({
        dna: ['ATGCGA', 'CAGTGC', 'TTATTT', 'AGACGG', 'GCGTCA', 'TCACTG'],
      });

      expect(hasMutation).toBe(false);
    });

    it('should hasMutation horizontal return true', () => {
      const { hasMutation } = appController.hasMutation({
        dna: ['ATGCGA', 'CAGTGC', 'AAAATT', 'AGACGG', 'GCGTCA', 'TCACTG'],
      });

      expect(hasMutation).toBe(true);
    });

    it('should hasMutation vertical return true', () => {
      const { hasMutation } = appController.hasMutation({
        dna: ['ATGCGA', 'AAGTGC', 'ATATTT', 'AGACGG', 'GCGTCA', 'TCACTG'],
      });

      expect(hasMutation).toBe(true);
    });

    it('should hasMutation diagonal return true', () => {
      const { hasMutation } = appController.hasMutation({
        dna: ['ATGCGA', 'CAGTGC', 'TTATTT', 'AGAAGG', 'GCGTCA', 'TCACTG'],
      });

      expect(hasMutation).toBe(true);
    });

    it('should hasMutation diagonal reverse return true', () => {
      const { hasMutation } = appController.hasMutation({
        dna: ['ATGCGA', 'CCGTGC', 'TTCTTT', 'AGACGG', 'GCGTCA', 'TCACTG'],
      });

      expect(hasMutation).toBe(true);
    });
  });
});
