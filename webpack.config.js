const webpack = require('webpack');
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: ['./src/main-firebase.ts'],
  target: 'node',
  stats: {
    warnings: false,
  },
  module: {
    rules: [
      {
        test: /.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  mode: 'development',
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.IgnorePlugin({
      checkResource(resource) {
        const lazyImports = [
          'cache-manager',
          'class-validator',
          'class-transformer',
          'class-transformer/storage',
          '@nestjs/websockets/socket-module',
          '@nestjs/microservices/microservices-module',
          '@nestjs/microservices',
          '@nestjs/common',
          'apollo-server-fastify',
          '@apollo/gateway',
        ];
        if (!lazyImports.includes(resource)) {
          return false;
        }
        try {
          require.resolve(resource);
        } catch (err) {
          return true;
        }
        return false;
      },
    }),
    new CopyPlugin({
      patterns: [{ from: 'package.json' }],
      options: {
        concurrency: 100,
      },
    }),
  ],
  output: {
    libraryTarget: 'this',
    path: path.join(__dirname, 'dist'),
    filename: 'index.js',
  },
};
